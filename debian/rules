#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

ifneq ($(findstring bpo,$(DEB_VERSION)),)
	FLAVOR := debian-backports
else ifneq ($(findstring pgdg,$(DEB_VERSION)),)
	FLAVOR := pgdg
else
	FLAVOR := default
endif
SUPPORTED_VERSIONS := $(shell PG_SUPPORTED_VERSIONS="$(FLAVOR)" debian/supported-versions)
DEFAULT_VER := $(lastword $(SUPPORTED_VERSIONS))

%:
	dh $@

override_dh_auto_configure:
	@echo "### Building postgresql-common flavor $(FLAVOR)"
	@echo "### Supported PostgreSQL versions: $(SUPPORTED_VERSIONS) (default version: $(DEFAULT_VER))"
	echo "# See /usr/share/postgresql-common/supported-versions for documentation of this file" > supported_versions
	echo "$(FLAVOR)" >> supported_versions

# do not restart postgresql.service on postgresql-common upgrades
override_dh_installinit:
	dh_installinit -ppostgresql-common --name=postgresql -u'defaults 19 21' --no-stop-on-upgrade
override_dh_installsystemd:
	dh_installsystemd --no-stop-on-upgrade
	# move unit files to /usr on systems that support it
	if test -x /usr/bin/dh_movetousr; then dh_movetousr; fi

override_dh_gencontrol:
	dh_gencontrol -ppostgresql-server-dev-all -- -Vserver-dev-all-depends="$(foreach v,$(SUPPORTED_VERSIONS),postgresql-server-dev-$v,)"

	# the versionless metapackages need to have version numbers which match
	# the server version, not the p-common version
	dh_gencontrol -ppostgresql -ppostgresql-client -ppostgresql-doc -ppostgresql-contrib -- \
		-Vdefault-version="$(DEFAULT_VER)" -v'$(DEFAULT_VER)+$${source:Version}'

	dh_gencontrol -ppostgresql-all -- \
		-Vpostgresql-all-depends="$(foreach v,$(SUPPORTED_VERSIONS),postgresql-$v,postgresql-plperl-$v,postgresql-plpython3-$v,postgresql-pltcl-$v,)"

	dh_gencontrol --remaining-packages
